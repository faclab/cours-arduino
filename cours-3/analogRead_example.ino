const int sensor_p = A4;

void setup() {
  Serial.begin(9600);
  pinMode(sensor_p, INPUT );
}

void loop() {
  int val = analogRead(sensor_p);
  Serial.println(val);
  delay(500);
}
